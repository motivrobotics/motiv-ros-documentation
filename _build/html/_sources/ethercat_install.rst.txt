.. _ethercat_install:

Installing and Configuring EtherCAT
===================================

This tutorial will walk you through installing and configuring EtherCAT for use with Motiv ROS software.


Installation
------------
1. Download the latest EtherCAT software

   .. code-block:: bash

	$ cd ~
	$ hg clone http://hg.code.sf.net/p/etherlabmaster/code ethercat-hg


2. Configure and build EtherCAT 


   .. code-block:: bash

	$ sudo apt-get install autoconf
	$ cd ethercat-hg
	$ ./bootstrap

	$ ./configure --with-linux-dir=/usr/src/linux-{your headers}/ --disable-8139too --enable-generic --disable-eoe --enable-tool

   .. note:: **Finding you Linux Headers**
   
      When running the ./configure command (above) you will need to insert the linux headers version on your particular machine. To check what you have running:

      .. code-block:: bash
         
         $ uname -r

      You can then copy the version number in place of {your headers}. As you type, press TAB to autocomplete the path to make sure you have the corect version in place.


3. Install

   .. note:: **Editing cdev.c for Building EtherCAT libraries**  

      The current version of EtherCAT will build with an error resulting from a call to an object's member in ethercat-hg/master/cdev.c (line 277):

      .. code-block:: c

         EC_MASTER_DBG(priv->cdev->master, 1, "Vma fault, virtual_address = %p,"
            " offset = %lu, page = %p\n", vmf->virtual_address, offset, page);
        
      You will need to change the call from **virtual_address** to **address**:

      .. code-block:: c
    
         EC_MASTER_DBG(priv->cdev->master, 1, "Vma fault, virtual_address = %p,"
            " offset = %lu, page = %p\n", vmf->address, offset, page);


   .. code-block:: bash

	$ make all modules
	$ sudo bash
	$ make modules_install install
	$ depmod -a


Configuration and Setup
-----------------------
1. Add and configure the necessary runfiles to your system:


   .. code-block:: bash

	$ cd ~/ethercat-hg
	$ sudo bash
	$ cp script/init.d/ethercat /etc/init.d
	$ chmod 755 /etc/init.d/ethercat
	$ ln -s /opt/etherlab/bin/ethercat /usr/local/bin/ethercat
	$ mkdir /etc/sysconfig
	$ cp script/sysconfig/ethercat /etc/sysconfig
	

2. Modify some files

  First, edit the ethercat file:

   .. code-block:: bash

	$ gedit /etc/sysconfig/ethercat
	
  Add generic to the DEVICE_MODULES parameter following:


   .. code-block:: bash

	DEVICE_MODULES = "generic"

  Next create and edit the file:

   .. code-block:: bash

	$ gedit /etc/udev/rules.d/98-ethercat

  Add the following:

   .. code-block:: bash	

	KERNEL=="EtherCAT[0-9]*" , MODE="0666"

  Restart ethercat:

.. todo:: !FIXME!  This results in an error "could not insert 'ec_master': Invalid argument"

   .. code-block:: bash

	$ /etc/init.d/ethercat restart


#. Modify libc so that Motiv ROS packages can find ethercat at runtime

   .. code-block:: bash

	$ sudo ldconfig
	$ sudo gedit /etc/ld.so.conf.d/libc.conf


   Add the following line:

   .. code-block:: bash

	/opt/etherlab/lib

   And run:

   .. code-block:: bash

	$ sudo ldconfig

