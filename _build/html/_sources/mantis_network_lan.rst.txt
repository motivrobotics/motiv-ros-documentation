.. _mantis_network_lan:

Configuring Robomantis for LAN
==============================

This tutorial will walk you through adding static IPs to Robomantis for use on a private LAN.

For this tutorial, you will need 3 dedicated static IPs:
	
	1. {high_brain_ip}

	#. {low_brain_ip}

	#. {operator_lan_ip}

	.. note:: **Operator IP**
		The operator_lan_ip must be on both the High Brain and Low Brain, along with all other operator IPs, in order for each machine to resolve the ROS_IP of the operator at runtime.

It is recommended to use relatively high values on your subnet (i.e. XXX.XXX.XXX.200 or higher).

High_Brain Config
^^^^^^^^^^^^^^^^^
SSH into the High Brain via the Wireless AP.

First, add the {high_brain_ip} and **high_brain** hostname, and the {operator_lan_ip} and **operator_lan** hostname to the /etc/hosts file:

	.. code-block:: bash

		$ sudo gedit /etc/hosts

	.. code-block:: bash

		{high_brain_ip}		high_brain
		{operator_lan_ip}	operator_lan

	.. note:: **IP and Hostname**

			The syntax for adding a static ip to your hosts file is:

				.. code-block:: bash

					{ip}		{hostname}

Restart networking:

	.. code-block:: bash

		$ sudo /etc/init.d/networking restart


Next, the connection profile for the High Brain must include the new static ip:

	.. code-block:: bash

		$ sudo gedit /etc/NetworkManager/system-connections/HighBridge

Add (or replace) the LAN ip in address1 with the {high_brain_ip}:

	.. code-block:: bash

		address1={high_brain_ip}/24,{gateway}

It is also necessary to add (or replace) the dns value with the DNS of the private LAN:

	.. code-block:: bash

		dns={local DNS}

Once edited, restart the network manager:

	.. code-block:: bash

		$ sudo /etc/init.d/network-manager restart

	.. note:: You may lose connectivity, but will be able to reconnect.

To test the new IP, ping the new ip from your local machine while on the LAN network:

	.. code-block:: bash

		$ ping {high_brain_ip}


Low_Brain Config
^^^^^^^^^^^^^^^^
SSH into the Low Brain, either through the Wireless AP or through the High Brain.

First, add the {low_brain_ip} and **low_brain** hostname, and the {operator_lan_ip} and **operator_lan** hostname to the /etc/hosts file:

	.. code-block:: bash

		$ sudo gedit /etc/hosts

	.. code-block:: bash

		{low_brain_ip}		low_brain
		{operator_lan_ip}	operator_lan


Restart networking:

	.. code-block:: bash

		$ sudo /etc/init.d/networking restart


Next, the connection profile for the High Brain must include the new static ip:

	.. code-block:: bash

		$ sudo gedit /etc/NetworkManager/system-connections/High_Brain\ Port

Add (or replace) the LAN ip in address1 with the {high_brain_ip}:

	.. code-block:: bash

		address1={low_brain_ip}/24,{gateway}

It is also necessary to add (or replace) the dns value with the DNS of the private LAN:

	.. code-block:: bash

		dns={local DNS}

Once edited, restart the network manager:

	.. code-block:: bash

		$ sudo /etc/init.d/network-manager restart

	.. note:: You may lose connectivity, but will be able to reconnect.

To test the new IP, ping the new ip from your local machine while on the LAN network:

	.. code-block:: bash

		$ ping {low_brain_ip}


Operator Config
^^^^^^^^^^^^^^^
On the operator machine, add the {operator_lan} IP to your /etc/hosts file:

	.. code-block:: bash

		$ sudo gedit /etc/hosts

	.. code-block:: bash

		{operator_lan}		operator_lan


You must also add this static ip, to the connection profile you are using to connect to LAN in the network manager of your system.


Finally add the {operator_lan} IP to the /etc/hosts files on both the Low Brain and High Brain (as you did in the previous steps. Make sure the hostname associated with this IP is **operator_lan**.


