.. _run_py:

The run.py Script
=================

This tutorial will walk you through the use of the run.py script. This utility allows you to launch various Robomantis nodes with desired parameters without having to run multiple independant ros .launch files.

The syntax for the script is as follows:

		 python run.py [-h] **model** **interface** [-p P] [-g G] [-c] [-v] [-f] 

The positional arguments are as follows:
	
	- **model**: rm1, rm2
	- **interface**: none, fake, csim, rosc, gsim

The optional arguments are as follows:

	- -h, show the help message and exit
	- -p, planner type: m=moveit (default), t=traclabs
	- -g, gui: r=rviz (default), g=gazebo, b=both, n=none
	- -c, run the armctl.py control GUI
	- -v, run the mantis_drive node for driving control
	- -f, run without affordance templates

Examples:

1. This sample execution of run.py will bring up a simulation of Robomantis in RViz with the MoveIT planner, driving and the armctl.py GUI:

	.. code-block:: bash

		python run.py rm1 csim -pm -gr -c -v

	Note that for the previous example, the MoveIT planner and RViz are default options for those parameters, therefore calling them explicitly in the command was unecessary. The following command is equivalent:

	.. code-block:: bash

		python run.py rm1 csim -c -v

#. This next example will bring up the ros control server to actually run hardware, using the TracLABs planner:

	.. code-block:: bash

		python run.py rm1 rosc -pt


