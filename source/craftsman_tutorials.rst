.. _craftsman_tutorials:

CRAFTSMAN Utilities
===================

The Craftsman suits is a set of utilities for motion planning. This includes teleoperation of limbs, affordance templates and waypoint navigation.

The following link will take to you to a set of tutorials on how to use these utilities: CRAFTSMAN_ 

		.. _CRAFTSMAN: https://traclabs.bitbucket.io/documentation/motiv/robomantis_rviz_tutorials.html
