.. _sim_examples:

Simulation Examples
===================

The following tutorial will walk you through some example simulations using the Robomantis software suite. The step-by-step instructions will give you an introduction into how both simulation and real-time control of the robot are executed. More in-depth pages follow this tutorial.

A Basic Launch
--------------

This first walkthrough will load a model of the robot in RViz and launch the armctl gui for robot control.

1. Copy the default.rviz configuration file from the workspace to the ~/.rviz directory:

	.. code-block:: bash

		cp ~/micra/src/default.rviz ~/.rviz/

#. Navigate to the source directory of your workspace:

	.. code-block:: bash

		cd ~/micra/src

#. Execute the run.py script with the following parameters:

	.. code-block:: bash

		python run.py rm1 csim -c

#. RViz will come up with a model of Robomantis loaded and the armctl.py gui will launch.

	.. image:: _static/rviz_arm_launch_VM.JPG
		:width: 500px
		:align: center 

Joint Moves
^^^^^^^^^^^
The armctl.py gui has controls on the left for controlling individual joints of the limb selected at the top (LF, RF, RR, etc.).

	.. image:: _static/Armctl_gui_VM.JPG
		:width: 500px
		:align: center


	.. images:: _static/arm_jointctl_VM.JPG

	.. note:: **The Virtual ESTOP**

		Before being able to execute any motion (either in sim or on hardware) the *Reset/Enable* button must be selected on either the RViz estop panel or armctl estop panel. This is an added safety. To halt motion immediately, the *HALT!* button can be used (on hardware it is always recommended to use the physical estop rather than the virtual one). 

Cartesian Moves
^^^^^^^^^^^^^^^
The armctl.py panel on the right is for executing motion along a limb that requires an kinematic solver. For example, you can perform a cartesian motion on the manipulation limb by:

	1. Selecting the Right Manipulation limb (RM at the top)

		.. image:: _static/arm_limbselect_VM.JPG
			:width: 500px
			:align: center

	2. Selecting Cartesian path in the right panel

		.. image:: _static/arm_cartesian_VM.JPG
			:width: 500px
			:align: center

	3. Jogging the Z parameter using the arrows.


If the pose is valid, the motion will execute.


Launch with Driving
-------------------
This next example will walk you through launching and running the driving software for the robot. This process uses the same run.py script with slightly different parameters.

1. Navigate to the source directory of your workspace:

	.. code-block:: bash

		cd ~/micra/src

#. Execute the run.py script with the following parameters:

	.. code-block:: bash

		python run.py rm1 csim -v

	This will start the necessary ros nodes in sim (hence the **csim** argument), load the robot model and the -v parameter will start the driving software.



Driving the Robot
^^^^^^^^^^^^^^^^^

To launch the direct drive GUI:

1. Type the following command into a new terminal:

	.. code-block:: bash

		rosrun motiv_motion_planning drive.py

	This command will launch the drive control gui. The panel on the right has buttons for driving in various directions and a slider to adjust speed and the panel to the left has buttons for in place rotation:

	.. image:: _static/drivepy_VM.JPG
		:width: 500px
		:align: center	

	The panel corresponds to the standard numeric keypad found on most keyboards. Once launched, this physical keypad can be used in lieu of the gui.

