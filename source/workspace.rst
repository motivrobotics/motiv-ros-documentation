.. _workspace:

The Robomantis Suite
====================

This tutorial will give you an overview of the newly built workspace containing all of the necessary software for running the Robomantis suite.

The Workspace
-------------

The various packages that make up the software to run/simulate Robomantis are located in the source (src) directory of your workspace (~/micra/src). 

The other folders found in the workspace (build, devel, logs) are part of the *catkin build*. *devel* contains the *setup.bash* file that is sourced in your **~/.bashrc** file. Sourcing this file allows you to find all built packages and launch all built executables. 


The Packages
------------
