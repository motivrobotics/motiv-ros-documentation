.. _create_vm:

Creating a Virtual Machine
==========================

This tutorial will show you how to set up a virtual machine for running Motiv ROS packages.

First, download and install `VirtualBox <https://www.virtualbox.org>`_.

Next, download the `ubuntu-16.04.3 iso <https://www.ubuntu.com/download/desktop>`_. This file will be used to install ubuntu on the virtual machine.

Set Up a New Virtual Machine
----------------------------
These steps will walk you through the process of setting up a VM that will run Motiv and Craftsman software.

1. Start up VirtualBox Manager

#. Select New

       .. image:: _static/New_VM.JPG
    	:width: 500px
     	:align: center

#. Enter a userful *Name* for your new VM


#. Under *Type* select **Linux**


#. Under *Version* select **Ubuntu (64-bit)**

       .. image:: _static/Type_Version_VM.JPG
     	:width: 500px
     	:align: center


#. Select *Next*


#. Allocate a minimum of **6GB** (6000MB) to your VM and select *Next*. 
   You can always adjust this setting later.

       .. image:: _static/Memory_VM.JPG
      	:width: 500px
   	:align: center


#. Select *Create a virtual hard disk now* and hit *Create*

       .. image:: _static/VirtualHD_VM.JPG
  	:width: 500px
     	:align: center


#. When prompted, select *VHD (Virtual Hard Disk)* and select *Next*

       .. image:: _static/VDType_VM.JPG
	:width: 500px
	:align: center


#. When prompted, select *Fixed size* and select *Next*

       .. image:: _static/FixedHD_VM.JPG
	:width: 500px
	:align: center


#. Allocate a mimimum of **30GB** to your VM and select *Create*. 

       .. image:: _static/VDSize_VM.JPG
	:width: 500px
	:align: center


       .. note:: **Once created, it is difficult to resize the partition.**


#. Once completed, select your newly created VM and hit *Settings*

       .. image:: _static/Settings_VM.JPG
	:width: 500px
	:align: center


#. Under *Network* -> *Adapter 1* and set *Attached to:* to **Bridged Adapter** and select your network adapter under the *Name* dropdown.

       .. image:: _static/NetworkSetting_VM.JPG
	:width: 500px
	:align: center


#. Under *Storage* -> *Controller:IDE* select the *Empty* drive


#. On the right-hand panel, select the disk icon next to *IDE Secondary Master* and *Choose Virtual Optical Disk File*

       .. image:: _static/SelectDrive_VM.JPG
     	:width: 500px
	:align: center


#. Navigate to and select your saved **ubuntu-16.04.3-desktop-amd64** iso file. Click *OK*.

       .. image:: _static/SelectISO_VM.JPG
     	:width: 500px
     	:align: center


#. *Start* your virtual machine and follow the prompts to install ubuntu.


