.. motiv-ros-core-docs documentation master file, created by
   sphinx-quickstart on Tue Mar 13 15:41:41 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to motiv-ros-core-docs's Documentation!
===============================================
This is where installation and user instructions can be found for all Motiv ROS Software.

.. toctree::
	:maxdepth: 2
	:caption: Contents:

	about 

	where_to_start

	create_vm

	mantis_suite_install    

 	workspace_overview

	robomantis_software

	robomantis_networking
	
	running_hardware



.. todo:: Install Linux on PC
          .. How to create an install drive

.. todo:: Writing your own nodes?


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
