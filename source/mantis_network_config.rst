.. _mantis_network_config:

Robomantis Network Configuration
================================

This page will give you an overview of how Robomantis computers are networked and how to appropriately access the different subsystems.

Brain Power
^^^^^^^^^^^
Robomantis has two computers on-board; the *low_brain* and *high_brain*. Functionally, the *high_brain* is intended to run all high-level planning software and the *low_brain* is dedicated to running all low-level limb activities. 

All subsystems on the robot are connected directly to the *high_brain* -- i.e. the *low_brain* and *mantispwrsystem*. The *high_brain* and all connected subsystems can be accessed via the following methods:

	- Wireless Access Point Connection
	- Direct Ethernet Connection
	- LAN Connection

All methods use static IPs for communication across subsystems and between the user and Robomantis. This requires setting up connections with Static IPs on the user machine. Additionally, the LAN method of connection, and internet, is required for making software updates.

The following table has the preconfigured IP addresses for connecting to the robot:
	
	
+------------+----------------+-----------------+----------------+------------------+-------------+
| Subsystem  | Network IPs    |                        Hostname                                   |
+============+================+=================+================+==================+=============+	
|            |  Access Point  | Direct Ethernet |  Access Point  |  Direct Ethernet |   LAN       |
+------------+----------------+-----------------+----------------+------------------+-------------+
| High Brain | 169.168.0.123  |   10.42.2.1     |   high_brain   |    high_brain    |  high_brain |
+------------+----------------+-----------------+----------------+------------------+-------------+
| Low Brain  | 169.168.0.141  |   10.42.2.3     |   low_brain    |    low_brain     | low_brain   |
+------------+----------------+-----------------+----------------+------------------+-------------+
| Operator   | 169.168.0.200  |   10.42.2.2     |   operator_ap  |   operator_eth   | operator_lan|
+------------+----------------+-----------------+----------------+------------------+-------------+


These IPs/hostnames should be in the /etc/hosts file on your operator machine:

	.. code-block:: bash

		#Rutgers Robomantis High_Brain
		169.168.0.123		high_brain #Access Point 
		10.42.2.1		high_brain #Direct Ethernet
		{high_brain_lan}		high_brain #LAN Bridge

		#Rutgers Low-Brain
		169.168.0.141		low_brain #Access Point
		10.42.2.3		low_brain #Direct Ethernet
		{low_brain_lan}		low_brain #LAN Bridge

		#Operator IP(s)
		169.168.0.200		operator_ap  #Access Point
		10.42.2.2		operator_eth #Direct Ethernet
		{operator_lan}		operator_lan #LAN

You will need to allocate 3 static LAN IPs for the high_brain, low_brain and operator machine to run ROS nodes over LAN.

The following section will show you how to add your LAN IPs to all machines.
