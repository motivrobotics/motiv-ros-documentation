.. _ros_install:

Installing ROS
==============

This tutorial will walk you through installing ROS on your system.

ROS Installation Steps
----------------------

1. Uncomment deb-src for restricted, universe and multiverse so that ros packages can be found by apt-get update

  .. code-block:: bash

     sudo gedit /etc/apt/sources.list


2. Add to ROS to your sources list:

   .. code-block:: bash

      sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'


3. Add the necessary key

   .. code-block:: bash

      sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116


4. Apt-Get update with new ROS sources

   .. code-block:: bash

      sudo apt-get update


5. Install ROS (and get yourself a coffee)

   .. code-block:: bash

      sudo apt-get install ros-kinetic-desktop-full


6. Use rosdep to install necessary dependencies

   .. code-block:: bash

      sudo rosdep init
      rosdep update
	

7. Setup your .bashrc file so that you can find installed ROS packages

   .. code-block:: bash

      echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
      source ~/.bashrc


Motiv ROS Required Packages
---------------------------

1. Install the following required packages to run with Motiv software

   .. code-block:: bash

      sudo apt-get install python-rosinstall python-rosinstall-generator python-wstool build-essential
      sudo apt-get install ros-kinetic-gazebo-ros-control
      sudo apt-get install ros-kinetic-moveit
      sudo apt-get install ros-kinetic-robot-self-filter
      sudo apt-get install ros-kinetic-move-base ros-kinetic-move-base-msgs
      sudo apt-get install ros-kinetic-industrial-msgs ros-kinetic-industrial-utils
      sudo apt-get install ros-kinetic-ros-control ros-kinetic-ros-controllers ros-kinetic-rqt-controller-manager
      sudo apt-get install python-pip
      sudo apt-get install ros-kinetic-opencv3
      sudo apt-get install ros-kinetic-cv-bridge
      sudo apt-get install ros-kinetic-rosdoc-lite
      sudo apt-get install ros-kinetic-depthimage-to-laserscan
      sudo apt-get install ros-kinetic-gazebo-plugins
      sudo apt-get install tcllib
      pip install --user pyassimp
2. Install CUDA 9.0, if any previous cuda installation exists, or your cuda install is corrupted, remove everything with:

   .. code-block:: bash
   
      sudo apt-get --purge remove "*cublas*" "cuda*"
      
   next, download cuda 9.0, NOT 9.1 or any other versions! There is a specific requirement for the ZED SDK version that you use.
   
   .. code-block:: bash
   
      wget https://developer.nvidia.com/compute/cuda/9.0/Prod/local_installers/cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64-deb
      sudo dpkg -i cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64.deb`
      sudo apt-key add /var/cuda-repo-<version>/7fa2af80.pub`
      sudo apt-get update`
      sudo apt-get install cuda-9-0`
      
3. Install ZED SDK v2.8.3, only for ubuntu 16.04!

   .. code-block:: bash
   
      wget https://download.stereolabs.com/zedsdk/2.8/ubuntu16_cuda9 
      chmod +x ubuntu16_cuda9
      ./ubuntu16_cuda9
      
   follow onscreen instructions, it should successfully detect CUDA. Use nvidia-nmi to verify the install
 