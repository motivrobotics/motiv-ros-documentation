.. _running_robot:

Running Robomantis
==================

To run Robomantis, you will first need to connect to it via wireless Access Point, Direct Ethernet, or LAN.

The instructions on how to connect to Robomantis can be found :ref:`here <mantis_network_ws>`.

Once connected to Robomantis, SSH into the High Brain:

	.. code-block:: bash

		$ ssh -X high_brain

Run rosmaster and nodes from the High Brain via the :ref:`run.py script <run_py>`.

RViz
^^^^


