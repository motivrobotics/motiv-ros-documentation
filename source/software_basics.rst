.. _software_basics:

Introduction to the Robomantis Software Suite:
=============================================

This page will familiarize you with the components that comprise the Robomantis Software Suite.

Overview
========

The Robomantis software suite contains packages for visualization, simulation and running of the Motiv Robomantis. With the tools in this suite you will be able to:
	
	1. Visualize Robomantis in its active state both in simulation and while running the actual robot.

	#. Peform joint level manipulations of the robot.

	#. Execute cartesian motions with the various limbs of the robot.

	#. Drive the robot manually.

	#. Drive along a predefined path.

	#. Manipulate virtual objects using the TracLABs Affordance Templates.
