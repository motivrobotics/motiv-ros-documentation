.. _update_software:

Keeping Software Up-To-Date
===========================

This tutorial will instruct you on how to update both debian packages and your workspace.

Updating Motiv-ROS-Release
^^^^^^^^^^^^^^^^^^^^^^^^^^
The workspace is linked to the Motiv-ROS-Core-Release repository. To see if anything has been updated, simply use the GIT command:

	.. code-block:: bash
		
		$ cd ~/micra/src
		$ git fetch origin master

Then check the status of the remote repository against what you have in your workspace:

	.. code-block:: bash
	
		$ git status

If you have local modifications, you will need to check them out or stash them before you can pull updates. To pull the latest:

	.. code-block:: bash

		$ git pull origin master

Once your workspace has been updated, you will need to rebuild and resource to implement changes:

	.. code-block:: bash

		$ cd ~/micra
		$ catkin build

	.. note:: **Rebuilding the Workspace**

		Anytime you make changes to files in your workspace that need compiling (e.g. .cpp files), you will need to rebuild your workspace to implement these changes. It is also recommended if there are significant changes to use *catkin clean* before *catkin build*.

Now resource your workspace in any open terminals so that you can find/use the newly compiled and built software:

	.. code-block:: bash

		$ source ~/.bashrc


Updating Debian Packages
^^^^^^^^^^^^^^^^^^^^^^^^
To update debian packages, either CRAFTSMAN or Motiv, you will need to modify the */etc/apt/s3auth.conf* containing the AWS keys for each set of packages depending on what you are trying to update.

First, uncomment the key set of the debian software you are trying to update and comment out the other. 

	.. note:: **You can only have one uncommented key set in the s3auth.conf file at a time!**

For example, if you wanted to update the Motiv debians:

Comment:
  .. code-block:: bash

     #CRAFTSMAN-DEB USER Credentials
     #AccessKeyId = {Craftsman Access Key}
     #SecretAccessKey = {Crafsman Secret Key}
     #Token =

Uncomment:
  .. code-block:: bash

     #MOTIV-ROS-CORE User Credentials
     AccessKeyId = {Motiv Access Key}
     SecretAccessKey = {Motiv Secret Key}
     Token = 

Now you can run normal apt update, apt install commands:

	.. code-block:: bash

		$ sudo apt update
		$ sudo apt install {debians}

The CRAFTSMAN **{debians}** are:
	ros-kinetic-craftsman

The Motiv **{debians}** are:
	ros-kinetic-ees
	ros-kinetic-motiv-limb
	ros-kinetic-mantis-nav
