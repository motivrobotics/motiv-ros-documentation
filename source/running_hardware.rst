.. _running_hardware:

Running Software on Robomantis
==============================

This set of tutorials will get you started running the actual Robomantis software. It should be noted that there are slight differences in the commands used between simulated software and software running on hardware. 

**CAUTION** should be used when running Robomantis. Always have the physical ESTOP ready.


.. toctree::
	 :maxdepth: 2
	 :caption: Contents:

	 Precautions <precautions>
	 Power System <power_system>
	 Running Robomantis <running_robot>
	 Quick-Start <rosc_examples>
	 Sensors	<sensors>
