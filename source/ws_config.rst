.. _ws_config:

Configuring Your Workspace
==========================

The ~/.bashrc
-------------
The bashrc file is where various parameters live that allow you to run ROS in a networked configuration. For simulation, much of this is unecessary, but when you begin running on actual hardware, this file will be where you will create and edit those parameters.

When setting up and building your workspace, the following lines were added to the bashrc file:

	.. code-block:: bash
     source /opt/ros/kinetic/setup.bash
     source $HOME/micra/devel/setup.bash

The bashrc file is one of the first places a bash terminal will look for references to relevant libraries, executables, etc. on startup. Adding the previous lines will allow execution of ros commands found in these directories. 

	.. note:: **TROUBLESHOOTING TIP**

		If you are experiencing errors where nodes are not launching and packages are not found, you may not be sourcing your ~/.bashrc file. To check run the following command:
	
	.. code-block:: bash
     $ echo $ROS_PACKAGE_PATH

		You should see a list of both installed and workspace packages. If not, run the following and check again:

	.. code-block:: bash
     $ source ~/.bashrc


Your ROS_IP and ROS_HOSTNAME
----------------------------
The ROS_IP and ROS_HOSTNAME is used to identify different machines in a network configuration. It is best practice to set the actual hostname and ip(s) associated with it in your /etc/hosts file. For example:

	.. code-block:: bash
     #/etc/hosts
		
     ros_machine1	192.168.1.2
     ros_machine2	192.168.1.3

The hostname and IP for **ros_machine1** are for the local machine running the ros master (for example), which can be found on the network by the slaved machine **ros_machine2**. To subscribe and publish to one another via ros nodes, each machine can *export* their hostname to their ROS_IP and ROS_HOSTNAME in the ~/.bashrc file.

	.. code-block:: bash
     #~/.bashrc

     export ROS_IP=ros_machine1
     export ROS_HOSTNAME=ros_machine1

The previous is an example of what the .bashrc file would look like for the master machine.

For more information, you can refer to either of the official ROS pages:
`ROS Network Setup <wiki.ros.org/ROS/NetworkSetup>`_
`ROS Multiple Machines <wiki.ros.org/ROS/Tutorials/MultipleMachines>`_


The ROS Master
--------------
In any networked configuration, a ros master must exist. This is the machine where the main roscore process is executed before any other nodes are launched. The master is set in the environment variable ROS_MASTER_URI. By default, this variable is set to:
	
	.. code-block:: bash
     ROS_MASTER_URI=http://localhost:11311

You can check it's value by typing in the following (as with any environment variable):

	.. code-block:: bash
     echo $ROS_MASTER_URI

You can set the master temporarily (for the bash session that is open), by using the following command:

	.. code-block:: bash
     export ROS_MASTER_URI=http://<**your_master_uri**>:11311

Or, permanently by adding the previous line to your .bashrc. 
