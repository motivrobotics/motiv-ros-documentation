.. _rosc_examples:

Running on Robomantis
=====================

Basic Launch with Armctl and Driving
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	.. code-block:: bash

		cd ~/micra/src
		python run.py rm1 rosc -gn -pb -c -v -f


Adding Robot Interaction Tools
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
After launching the previous command, you can use the following roslaunch to teleoperate individual robot limbs.

	.. code-block:: bash

		roslaunch craftsman_tutorials robomantis1_RIT.launch gazebo:=false


Adding Affordance Templates
^^^^^^^^^^^^^^^^^^^^^^^^^^^
After launching the previous commands, you can use the following roslaunch to bring up affordance templates.

	.. code-block:: bash

		roslaunch craftsman_tutorials robomantis1_AT.launch


Adding Navigation Planning
^^^^^^^^^^^^^^^^^^^^^^^^^^
After launching the previous commands, you can use the following roslaunch to bring up navigation planning via waypoints.

	.. code-block:: bash

		roslaunch craftsman_tutorials drive_node.launch gazebo:=false

	.. note:: **SET YOUR POSE**

			If using the run.py **-v** parameter prior to this roslaunch, the first driving node will be closed and a new node started. Be sure to re-select the **Set Pose** button on the drive.py gui before running any manual drive commands.


Starting the Velodyne
^^^^^^^^^^^^^^^^^^^^^
To start the velodyne node and visualization in RViz, run the following roslaunch command:

		.. code-block:: bash

			roslaunch velodyne_pointcloud VLP16_points.launch

Make sure that the Velodyne Display is enabled under RViz displays. If not under displays by default, at the bottom of the displays panel select Add->PointCloud2. Then, under the newly added display select the topic dropdown and select velodyne.
