.. _install_debs:

Installing Motiv ROS debians
============================

This tutorial will walk you through the installation of three Motiv ROS packages:
	ros-kinetic-ees
	ros-kinetic-motiv-limb
	ros-kinetic-mantis-nav

And the TracLABs Craftsman software suite.

Setting Up AWS
--------------
First, you will need to set up AWS credentials for accessing the repositories:

1. Pull key files from bitbucket:

  .. code-block:: bash

     cd ~
     git clone https://bitbucket.org/motivrobotics/motiv-ros-user-keys.git motiv-keys

#. Add the *credentials* file in your homespace:

  .. code-block:: bash
     
     cd ~/motiv-keys
     cat credentials | sudo tee -a /etc/apt/s3auth.conf

#. Now install the AWS software required for pulling from Motiv servers

  .. code-block:: bash
     
     sudo apt-get install apt-transport-s3

Installing the Craftsman Suite
------------------------------
Previously we used apt-boto-s3 as our apt s3 plugin, but it kept hanging while holding dpkg/apt locks which effected all other package install. We
did not get good response from the developers, so we swtiched to a more mainstream solution. If you already installed that, here is how to
uninstall that first.

1. Removing the previous client:

  .. code-block:: bash

     rm -rf ~/.aws
     sudo apt-get purge apt-boto-s3
     sudo rm /etc/apt/sources.list.d/lucidsoftware-bintray.list
     sudo rm /etc/apt/sources.list.d/craftsman.list
     sudo apt-key del 379CE192D401AB61


#. Add the Craftsman gpg key:

  .. code-block:: bash

     cd ~/motiv-keys
     cat craftsman.key | sudo apt-key add -

#. Add the Craftsman AWS bucket to your sources list:
  
  .. code-block:: bash
		 
     echo 'deb s3://craftsman-binaries.s3.amazonaws.com/ stable main' | sudo tee -a /etc/apt/sources.list.d/craftsman.list

#. Apt-Get Update to find the Craftsman software:

  .. code-block:: bash
     
     sudo apt-get update

#. Install the packages!:

  .. code-block:: bash
     
     sudo apt-get install ros-kinetic-craftsman

Installing the Motiv Packages
-----------------------------

1. Uncomment the Motiv AWS keys and comment out the Craftsman keys in your credentials file:

  .. code-block:: bash

     sudo gedit /etc/apt/s3auth.conf

Uncomment:
  .. code-block:: bash

     #CRAFTSMAN-DEB USER Credentials
     #AccessKeyId = {Craftsman Access Key}
     #SecretAccessKey = {Crafsman Secret Key}
     #Token =

Comment:
  .. code-block:: bash

     #MOTIV-ROS-CORE User Credentials
     AccessKeyId = {Motiv Access Key}
     SecretAccessKey = {Motiv Secret Key}
     Token = 

Save and Close the file.

#. Add the Motiv gpg key

  .. code-block:: bash
     
     cd ~/motiv-keys
     cat motiv-ros.key | sudo apt-key add -

#. Add the AWS bucket to your sources list:

  .. code-block:: bash
     
     echo 'deb s3://motiv-ros.s3.amazonaws.com/ stable main' | sudo tee -a /etc/apt/sources.list.d/motiv-ros-core.list

#. Apt-Get Update to find all of the Motiv software

  .. code-block:: bash
     
     sudo apt-get update

#. Install the packages!

  .. code-block:: bash
     
     sudo apt-get install ros-kinetic-ees ros-kinetic-motiv-limb ros-kinetic-mantis-nav



