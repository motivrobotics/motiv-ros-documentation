.. _where_to_start:

Where to Begin
==============

The following set of tutorials will walk you through the installation and setup of Motiv ROS software. The process involves the following steps:

	1. Setting up a machine to run all of the software. This can be done either on a dedicated Linux (Ubuntu) machine or VM.
	
	#. Installing ROS

	#. Installing and Configuring EtherCAT

	#. Installing Motiv debian packages

	#. Installing TracLABs' Craftsman suite

	#. Pulling the latest Motiv ROS repository

	#. Building and Configuraing your workspace

If you plan to use a Virtual Machine, please start at the :ref:`create_vm` tutorial. Otherwise, jump straight into :ref:`ros_install`. 
