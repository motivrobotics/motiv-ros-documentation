.. _package_list:

Robomantis Package List
=======================
This page will give an overview of what each Robomantis package does.


The Robomantis Workspace:
-------------------------

The run.py Script
^^^^^^^^^^^^^^^^^
Normally, nodes in ROS are started using markup (.launch) files that require parameters specific to those nodes and their intended operation. The run.py script is a utility that takes a set of inputs and launches all necessary ROS nodes to run Robomantis in a required configuration without the need for multiple .launch files.

For details on how to use run.py, see :ref:`the run.py page <run_py>`.

Motiv Robot Models
^^^^^^^^^^^^^^^^^^
The motiv_robot_models package contains all urdf files to describe the physical geometry of Robomantis for use in ROS. This includes sensor models and interial values for each limb as well as the body.

Mantis CAM
^^^^^^^^^^
The mantis_cam package contains a launch file for configuring and starting the nodes that run camera data for use in ROS. 

	.. code-block:: bash
		
		roslaunch mantis_cam mantis_cam.launch

To launch this file, there must be a roscore node running as well as the robot state description of robomantis with the URDF in place (i.e. run.py).

Once running, the mantis_cam nodes will provide data from the cameras as well as allow you to visualize this data in RViz.

Motiv RViz Plugins
^^^^^^^^^^^^^^^^^^
The motiv_rviz_plugins package contains ESTOP and Force Torque Sensor plugins that are displayed in RViv. The virtual ESTOP panel allows you to immediately HALT and re-enable motion. The FTS plugin displays sensor data from available force torque sensors.

In order to use the FTS panel, ros nodes must be running on hardware with an FTS connected. 

Motiv Motion Planning
^^^^^^^^^^^^^^^^^^^^^
The motiv_motion_planning package contains .yaml configuration files for use when launching Robomantis. This includes joint_limits, hardware controller parameters and kinematics descriptions. 

The package also contains several utilities for running the robot, including **drive.py** and **armctl.py**. 

**drive.py** is a teleoperation tool that interfaces with mantis_nav and gives a user the ability to drive (wheel motion and steering) Robomantis. With the necessary ROS nodes, including mantis_nav, launched you can start drive.py:

	.. code-block:: bash
		
		rosrun motiv_motion_planning drive.py


**armctl.py** is a teleoperation tool for executing joint level, cartesian and scripted commands on the robot.

	.. code-block:: bash
		
		rosrun motiv_motion_planning armctl.py


.. Craftsman Tutorials
		^^^^^^^^^^^^^^^^^^^


.. Debian Packages:
		---------------

.. Motiv Limb/EES
		^^^^^^^^^^^^^^


.. Mantis Nav
		^^^^^^^^^^
