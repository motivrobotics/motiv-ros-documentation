.. _mantis_network_ws:

Configuring the WS to Connect
=============================

This page will walk you through how to set up your machine to connect with Robomantis.

Mantis IPs
^^^^^^^^^^
The static IPs tabulated in the previous section need to be added to your machine. To do this you will need to append your /etc/hosts file.

	.. code-block:: bash
		
		$ sudo gedit /etc/hosts

Add the IP/Hostnames to your file. It should look like this:

	.. code-block:: bash

		#Rutgers Robomantis High_Brain
		169.168.0.123		high_brain #Access Point 
		10.42.2.1		high_brain	#Direct Ethernet

		#Rutgers Low-Brain
		169.168.0.141		low_brain	#Access Point
		10.42.2.3		low_brain	#Direct Ethernet

		#ROS IP(s)
		169.168.0.200		operator_ap  #Access Point
		10.42.2.2		operator_eth	#Direct Ethernet	

Now, restart networking:

	.. code-block:: bash

		$ sudo /etc/init.d/networking restart


Setting Up Your ~/.bashrc
^^^^^^^^^^^^^^^^^^^^^^^^^
The ROS_MASTER_URI, ROS_IP and ROS_HOSTNAME need to be set in your ~/.bashrc so that when connected to Robomantis and running ROS nodes, the operator station will be able to communicate with those nodes. 

Open your ~/.bashrc for editing:

	.. code-block:: bash

		$ gedit ~/.bashrc

Add the following lines:

	.. code-block:: bash
	
		#ROS MASTER(s)
		export ROS_MASTER_URI=http://high_brain:11311
		#export ROS_MASTER_URI=http://localhost:11311

		#Operator Hostnames
		export ROS_IP=operator_ap
		#export ROS_IP=operator_eth
		#export ROS_IP=operator_lan
		#export ROS_IP=localhost
		
		export ROS_HOSTNAME=$ROS_IP


The ROS_MASTER_URI sets the IP address/hostname of the machine running the rosmaster node. In the case of Robomantis, the rosmaster runs on the High Brain. 

The ROS_IP of a machine identifies that machine to all other machines on the ROS network. Because we have set up the /etc/hosts file with all necessary IP/Hostname sets, we can simply designate the ROS_IP using the hostname that corresponds to the IP of the connection we are using; e.g. operator_ap when we are connected to Robomantis via Access Point.


Connecting via Access Point
^^^^^^^^^^^^^^^^^^^^^^^^^^^
To connect to Robomantis via wireless Access Point:

	1. Connect to SSID: 'Robomantis' using your network manager
	
	2. Once connected, set the static IP of the connection profile to:
		IP: 169.168.0.200
		Netmask: 255.255.255.0
		Gateway: 169.168.0.1
	
	3. Make sure your ~/.bashrc file has the correct ROS_IP hostname (i.e. operator_ap)

Connecting via Direct Ethernet
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To connect to Robomantis via wired Ethernet:

	1. Connect the operator station to Robomantis via ethernet
	
	2. Once connected, set the static IP of the connection profile to:
		IP: 10.42.2.2
		Netmask: 255.255.255.0
		Gateway: 10.42.2.1
	
	3. Make sure your ~/.bashrc file has the correct ROS_IP hostname (i.e. operator_eth)


