.. _motiv_release:

Building a Workspace with Motiv ROS Software
============================================

This tutorial will cover pulling Motiv software and building your workspace to run Motiv Software.

Install Required Software
-------------------------

1. Install Git

  .. code-block:: bash
     
     sudo apt-get install git

2. Clone 3rd party repos to your workspace. Make sure to add these repo to your gitignore
   
   .. code-block:: bash
      
      cd ~/micra/src                      # Navigate to the source space
      git clone https://github.com/stereolabs/zed-ros-wrapper.git      # Zed ROS Wrapper
      git clone https://github.com/AprilRobotics/apriltag.git      # Clone Apriltag library
      git clone https://github.com/AprilRobotics/apriltag_ros.git  # Clone Apriltag ROS wrapper
      git clone git@gitlab.energid.net:motiv_robotics/motiv_robotics.git energid #energid SSH only, need a gitlab account with RSA key
      cd ~/micra                          # Navigate to the workspace
      rosdep install --from-paths src --ignore-src -r -y  # Install any missing packages
      catkin build    # Build all packages in the workspace (catkin_make_isolated will work also)
      

Set Up Your Workspace
---------------------

1. Create a workspace

    .. code-block:: bash
     
     cd ~
     mkdir micra


#. Pull from Motiv-Robotics-ROS-Core Release

    .. code-block:: bash
     
     cd ~/micra
     mkdir src
     git clone https://bitbucket.org/motivrobotics/motiv-ros-core-release.git src


#. Misc Configs:

    .. code-block:: bash
     
     cd ~/micra
     touch src/hydro/CATKIN_IGNORE


Build all Packages
------------------

1. Install Catkin tools 
 
    .. code-block:: bash
     
     sudo apt-get install python-catkin-tools python-rosdep rng-tools python-pip dctrl-tools 
     sudo pip install clint


#. Config Build

    .. code-block:: bash
     
     source /opt/ros/kinetic/setup.bash
     catkin config --init --no-install --no-extend


#. Build Workspace

    .. code-block:: bash
     
     rosdep update
     rosdep install --from-path src --ignore-src -y
     catkin build

#. Add source spaces to your ~/.bashrc file:
	
    .. code-block:: bash

     gedit ~/.bashrc
   
 
   Add the following lines to the bottom of the file

      .. code-block:: bash

       source /opt/ros/kinetic/setup.bash
       source /usr/share/gazebo/setup.sh
       source $HOME/micra/devel/setup.bash


#. Source your newly created workspace:

    .. code-block:: bash
     
     source ~/.bashrc
