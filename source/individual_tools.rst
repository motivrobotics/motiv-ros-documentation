.. _individual_tools:

Robomantis Tools:
=================

This page will walk you through the various tools available in the Robomantis Software Suite.

Armctl.py GUI
-------------

The armctl.py gui is a an interface for executing joint level control, cartesian motion and scripting of motion sequences.

To launch:
	
	1. You must have ros running with either the real or simulated ethercat server and a Robomantis model loaded, e.g. via the following run.py command:

			.. code-block:: bash
		 
				python run.py rm1 csim

	#. Run the following command:

			.. code-block:: bash

				rosrun motiv_motion_planning armctl.py

		NOTE: The run.py command used previously can be run with the -c parameter to automatically bring up the armctl.py gui.


The ESTOP Panel
^^^^^^^^^^^^^^^
There is an ESTOP panel available for both RViz and the armctl.py gui. This panel has two buttons: **HALT!** and **Reset/Enable**. These act as virtual estops; an extended precautionary measure on top of a physical estop. 

	.. note:: **ESTOP and Executing ANY Motion**
		
		It is important to note that the **Reset/Enable** button on the virtual ESTOP must be selected after initial launch of software prior to any motion, otherwise motion will NOT execute. When the reset is selected, the button will highlight green, indicating that motion can be executed. If there is a fault, the button will highlight yellow.

The Joint Control Panel
^^^^^^^^^^^^^^^^^^^^^^^
The left-hand side panel of the armctl gui is used to control individual joint on the robot. 

	.. image:: _static/Armctl_gui_VM.JPG
			:width: 500px
			:align: center

- First, you must select the limb (kinematic chain) you wish to manipulate in the upper right-hand corner:

		.. image:: _static/arm_limbselect_VM.JPG
			:width: 500px
			:align: center

- The current position of each joint for the selected limb is shown on the panel:

		.. image:: _static/arm_jointctl_VM.JPG
			:width: 250px
			:align: center

- You can manually enter a new position you wish to drive the joint to in the provided text box and hit ENTER.


- You can also jog each joint using the provided arrows in the panel and set the jog increment in the text box next to jog.


	.. note:: **All Values in the Joint Control Panel are in Degrees** 


Cartesian Motion
^^^^^^^^^^^^^^^^
The right-hand side panel of the armctl gui is used to perform cartesian motions.

		.. image:: _static/arm_cartesian_VM.JPG
			:width: 300px
			:align: center

- First, you must select the limb (kinematic chain) you wish to manipulate in the upper right-hand corner.


- You can then manipulate the limb using x,y,z,r,p,y parameters with controls similar to the ones found in the joint control panel.



Running Sequences
^^^^^^^^^^^^^^^^^
It is also possible to script the commands you might use in the armctl gui into a canned sequence. 

	.. image:: _static/arm_speedsequence_VM.JPG
		:width: 500px
		:align: center

The field at the bottom of the panel is used to enter the file location of a sequence. The **Run Sequence** button will then execute the script. The syntax for a script and an example is given below:

	- path("cartesian") or path("joint") – set trajectory path mode
	- limb(name) – select planning/control limb group
	- joint(speed, pos, pos, ...) – absolute FK joint pose, speed = percent of max, pos = joint angles in degrees
	- rjoint(speed, pos, pos, ...) – relative FK joint pose, speed = percent of max, pos = joint angles in degrees
	- gjoint(group_name, speed, pos, pos, ...) – absolute joint pose for arbitrary group, speed = percent of max, pos = joint angles in degrees or "-" for (don't change)
	- base(speed, x, y, z, roll, pitch, yaw) – absolute IK/cartesian pose in base frame, speed = percent of max, x/y/z=tool position in meters, roll/pitch/yaw = tool orientation in degrees
	- rbase(speed, x, y, z, roll, pitch, yaw) – relative IK/cartesian pose in base frame, speed = percent of max, x/y/z=tool position in meters, roll/pitch/yaw = tool orientation in degrees
	- rtool(speed, x, y, z, roll, pitch, yaw) – relative IK/cartesian pose in tool frame, speed = percent of max, x/y/z=tool position in meters, roll/pitch/yaw = tool orientation in degrees
	- wait(milliseconds) – pause sequence for fixed time
	- wait() – wait for current motion to finish (otherwise a new pose interrupts the current motion)
	- abort(string) - abort script with message
	- msg(string) - display message in GUI status line, also print to stdout

	The "Copy Pose" command button in the arm control GUI copies the current pose (as a "base" command) to the clipboard. "Copy Joints" copies the current joint state (as a "joint" command).

	An Exception is raised if estop is active when a joint or pose command is requested. Unless caught with a try block, this aborts the script.

	Currently, wait() (with no time value) makes the GUI (including ESTOP!) unresponsive during the motion.



	.. note:: **Example: (also see mcmoveit/src/seq/)**

		.. code-block:: python
	
				print "Example arm sequence"
				limb("Arm")
				path("joint")
 
				joint(100, -30, -20, -20, 20, 0, 75)
				wait()
				rjoint(100, 1.1, 2.2, 3.3)
 
				base(50, 0, 0.1, 1.05, 0, 0, 180)
				wait()
				rbase(50, 0, 0.0, -0.05, 0, 00, 0)
				rtool(50, 0.0, -0.0, -.00, 0, -5, 0)
				wait()
 
				rtool(50, 0.0, -0.0, -.00, 0, -5, 0)
				wait(100)
				rtool(50, 0.0, -0.0, -.02, 0, -0, 0)


Drive.py GUI
------------
The drive.py gui is a an interface for executing wheel motion and steering.

	.. image:: _static/drivepy_VM.JPG
			:width: 500px
			:align: center

To launch:
	
	1. You must have ros running with either the real or simulated ethercat server and a Robomantis model loaded, as well as the mantis_drive node running; e.g. via the following run.py command:

			.. code-block:: bash
		 
				python run.py rm1 csim -v

	#. Run the following command:

			.. code-block:: bash

				rosrun motiv_motion_planning drive.py

		NOTE: The run.py command used previously MUST be run with the -v parameter to run any driving on the robot.



RViz Panels
-----------
There are several panels in RViz that have utility in either running or visualizing data with respect to the robot. It is recommended that you load the mantis_displays.rviz configuration file as this comes with all RViz configurations for running Robomantis. You can always add/remove utilities from your configurations file. To load, either:

	- Go to File->Open Config and select ~/micra/src/mantis_displays.rviz. 
		.. note:: **This method will require selection everytime you open RViz**

	- Copy mantis_displays.rviz to your .rviz directory:
		
		.. code-block:: bash
			
			cp ~/micra/src/mantis_displays ~/.rviz/default.rviz
		
		.. note:: **This second method will load the rviz configuration everytime you start RViz**

A summary of each panel/utility is outlined below:
 
Displays
^^^^^^^^

	.. image:: _static/rviz_Displays_VM.JPG
			:width: 250px
			:align: center

The displays panels is populated with different visualizations of data pertaining to the robot. For more information on these visualizations visit the ROS RViz_ page.

	.. _RViz: http://wiki.ros.org/rviz

A brief description of preloaded displays and what they do is listed below:

	- RobotModel: A model of Robomantis built from the URDF that corresponds to the robot's current (joint) state

	- RobotPlanningModel: A transparent model similar to the RobotModel that will display planned motions before they are executed.

	- NavPath: A display of the navigation path when the navigation_planner is enabled

	- RM_FTS: A visualization of the force and torque data coming from the sensor used on the end of the right manipulation limb (only on physical robot, not in sim).

	- ZED_{N}: A point-cloud display from the select ZED camera (physical robot only).

	- InteractiveControls: Interactive display of end-effector markers for manipulation of robot limbs.

	- AffordanceTemplateMarker: Interactive display of Affordance Templates

	- NavMarker: Interactive display of navigation marker


	.. ESTOP Panel
			^^^^^^^^^^^

	..	FTS Panel
		^^^^^^^^^

	.. Robot Interaction Tools
		^^^^^^^^^^^^^^^^^^^^^^^

	.. Affordance Templates
		^^^^^^^^^^^^^^^^^^^^
